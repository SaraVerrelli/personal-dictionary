<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WordController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\LanguageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'home'])->name('home');
Route::get('/new.language', [LanguageController::class, 'create'])->name('addLanguage');
Route::get('/{language}/new.word', [LanguageController::class, 'addNewWord'])->name('addWord');
Route::post('/store.language', [LanguageController::class, 'store'])->name('storeLanguage');
Route::post('/store.word/{language}', [LanguageController::class, 'storeWord'])->name('storeWord');
Route::get('/languages/show/{language}', [LanguageController::class, 'show'])->name('showLanguage');
Route::get('/languages/index', [LanguageController::class, 'index'])->name('allLanguages');
Route::delete('/language/{word}/delete', [LanguageController::class, 'destroyWord'])->name('deleteWord');
Route::delete('/{language}/delete', [LanguageController::class, 'destroy'])->name('deleteLanguage');
