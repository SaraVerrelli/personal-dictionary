<x-layout>
  <main class="container-fluid">
    <div class="row">
      @if (session('message'))
      <div class="alert alert-success">
          {{session('message')}}
      </div>
      
      @endif
  </div>
    <div class="row mx-auto">
      <h2>{{$language->name}}</h2>
      <h5>{{$language->use}}</h5>
      <a href="{{route('addWord', compact('language'))}}">Aggiungi una nuova parola</a>
      <div class="col">
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
          Elimina
        </button>
      </div>
      
    </div>
    <hr>
    <div class="row mx-auto">
      <h3>Le parole associate</h3>
      @foreach ($words as $word)
      <div class="col-12 col-sm-6 col-lg-3 mt-3">
        <div class="flip-card">
          <div class="flip-card-inner">
            <div class="flip-card-front">
              <h4 class="my-5">{{$word->word}}</h4>
            </div>
            <div class="flip-card-back">
              <h4 class="mt-5">{{$word->translation}}</h4>
              <p class="mb-5">{{$word->example}}</p>
              
            </div>
          </div>
        </div>
        
        <form method="POST" action="{{route('deleteWord', compact('word'))}}">
          @csrf
          @method('delete')
          <button type="submit" class="btn btn-info ms-3">Elimina</button>
        </form>
        
      </div>
      @endforeach
      <!-- Button trigger modal -->
           
      
      <!-- Modal -->
      <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              Eliminando la lingua eliminerai tutte le parole collegate, vuoi procedere?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Chiudi</button>
              <form method="POST" action="{{route('deleteLanguage', compact('language'))}}">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-info ms-3">Elimina</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  
</x-layout>