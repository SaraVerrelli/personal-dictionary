<x-layout>
    <div class="container-fluid my-5">
        <div class="row">
            @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
        </div>
    </div>
    <main class="container-fluid">
        <div class="row mx-auto">
            <div class="col-12 col-md-6 offset-md-3">
                <form method="POST" action="{{route('storeLanguage')}}">
                    @csrf
                    <div class="mb-3">
                      <label class="form-label">Dai un nome a questa lingua</label>
                      <input type="text" class="form-control" name="name">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">In quale ambito o a quale livello si può usare questo dizionario?</label>
                        <input type="text" class="form-control" name="use">
                      </div>
                    <button type="submit" class="btn btn-primary">Aggiungi</button>
                  </form> 

            </div>
        </div>
    </main>
</x-layout>