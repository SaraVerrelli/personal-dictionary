<x-layout>
    <main class="container-fluid">
        <div class="row">
            @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
        </div>
        <div class="row mx-auto">
            
            <h1>Le tue lingue</h1>
            @foreach ($languages as $language)
            <div class="col-6 col-sm-4 col-md-2">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{$language->name}}</h5>
                        <p class="card-text">Scopri quali parole hai aggiunto per questa lingua e aggiungine di nuove.</p>
                        <a href="{{route('showLanguage', compact('language'))}}" class="card-link">Vai a questa lingua</a>
                    </div>
                </div>
            </div>
            @endforeach
            
            
        </div>
    </main>
</x-layout>