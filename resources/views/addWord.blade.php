<x-layout>
    <div class="container-fluid my-5">
        <div class="row">
            @if (session('message'))
            <div class="alert alert-success">
                {{session('message')}}
            </div>
            
            @endif
        </div>
    </div>
    <main class="contaner-fluid">
        <div class="row mx-auto">
            <div class="col-12 col-md-6 offset-md-3">
                <form method="POST" action="{{route('storeWord', compact('language'))}}">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Parola</label>
                        <input type="text" class="form-control" name="word">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Traduzione</label>
                        <input type="text" class="form-control" name="translation">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Fai un esempio</label>
                        <input type="text" class="form-control" name="example">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Aggiungi</button>
                </form> 
            </div>
        </div>
    </main>
</x-layout>