<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>Your dictionary</title>
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    @auth
                    <li class="nav-item mx-2">
                        <p class="nav-link text-dark">Ciao {{Auth::user()->name}}</p>
                        
                    </li>
                    @endauth
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="{{route('home')}}">Home</a>
                    </li>
                    @guest
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="{{'login'}}">Login</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="{{route('register')}}">Registrati</a>
                    </li>
                    @endguest
                     
                    @auth
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="{{route('addLanguage')}}">Aggiungi una nuova Lingua</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="{{route('allLanguages')}}">Le tue lingue</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark mx-2" href="{{route('logout')}}" onclick="event.preventDefault();
                        document.getElementById('form-logout').submit();">Logout</a>
                        <form method="POST" action="{{route('logout')}}" id="form-logout">
                            @csrf
                        </form> 
                        
                    </li> 
                    
                    @endauth
                    
                </ul>
            </div>
        </div>
    </nav>
    
    
    {{$slot}}
    
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>