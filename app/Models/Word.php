<?php

namespace App\Models;

use App\Models\Language;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Word extends Model
{
    use HasFactory;
    protected $fillable = [
        'word', 'translation', 'example'
    ];


    public function language()
    {
        return $this->belongsTo(Language::class);
    }
}
