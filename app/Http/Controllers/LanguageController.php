<?php

namespace App\Http\Controllers;

use App\Models\Language;
use App\Models\Word;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LanguageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $languages = Auth::user()->languages()->get();
        return view('dashboard', compact('languages'));
    }

    public function create()
    {
        return view('addLanguage');
    }


    public function store(Request $request)
    {
        $user = Auth::user();
        $user->languages()->create([
            'name' => $request->name,
            'use' => $request->use,

        ]);

        return redirect(route('addLanguage'))->with('message', Auth::user()->name . ', la tua lingua è stata salvata');
    }

    public function show(Language $language)
    {
        $language = Auth::user()->languages()->find($language->id);
        $words = $language->words()->get();

        return view('showLanguage', compact('language', 'words'));
    }


    public function edit(Language $language)
    {
    }


    public function update(Request $request, Language $language)
    {
        //
    }


    public function destroy(Language $language)
    {
        $language = Auth::user()->languages()->find($language->id);
        $language->words()->delete();
        $language->delete();

        return redirect(route('allLanguages'))->with('message', 'La tua lingua è stata eliminata');
    }

    //Word functions

    public function addNewWord(Language $language)
    {

        return view('addWord', compact('language'));
    }

    public function storeWord(Language $language, Request $request)
    {

        $language->words()->create([
            'word' => $request->word,
            'translation' => $request->translation,
            'example' => $request->example,
        ]);

        return redirect(route('addWord', compact('language')))->with('message', 'La tua parola è stata inserita');
    }

    public function destroyWord(Word $word)
    {
        //$language = $word->language()->get();
        $word->delete();
        return $this->index();
    }
}
